Flask-WTF==1.2.1
Flask==3.0.0
py-cpuinfo==9.0.0
psutil==5.8.0
gunicorn==20.1.0
black==20.8b1
flake8==6.1.0
pytest==6.2.2